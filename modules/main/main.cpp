#include <iostream>
#ifdef _WIN32
#include <windows.h>
#endif // _WIN32
#include "crossplatform.h"
#include <fstream> //讀寫文件
#include <filesystem> // C++17 檔案系統

namespace fs = std::filesystem;
const char endl = '\n';

class Kyu {
public:
	inline static int level = 8; //C++17 inline variable
	std::string m_kyudir = ""; //C++11 non-static data member with default member initializer
	std::string m_title = "";
	fs::path m_path = "";
};

void OutputBase(std::ofstream & ofs) {
	ofs << "# CodeWars Solutions\n\n";
	ofs << "記錄[CodeWars](https://www.codewars.com/) C++的解法\n\n";
	ofs << "透過[C++程式](https://gitlab.com/nt761523/codewarsautoreadme)自動更新README.md\n\n";
	ofs << "[![Profile badge](https://www.codewars.com/users/nt761523/badges/large)](https://www.codewars.com/users/nt761523)\n\n";
}

void RWfile() {
	std::ofstream ofs("README.md", std::ios::out);
	OutputBase(ofs);

	auto filepath = fs::current_path();

	Kyu curkyu;
	std::ifstream ifs;
	do {
		curkyu.m_kyudir = std::to_string(curkyu.level) + " kyu";
		fs::path curdir = filepath / "C++" / curkyu.m_kyudir;

		if (fs::exists(curdir)) {
			ofs << "## " << curkyu.m_kyudir << "\n";
			ofs << "---" << "\n";
			for (const auto& p : fs::directory_iterator(curdir)) {
				curkyu.m_path = p;
				curkyu.m_title = curkyu.m_path.stem().string();


				ifs.open(curkyu.m_path, std::ios::in);
				if (!ifs.is_open()) {
					std::cout << "開啓文件： " << curkyu.m_path << " 失敗！";
					return;
				}
				std::string buf;
				std::getline(ifs, buf);
				buf = buf.substr(1, buf.length());
				ofs << '[' << curkyu.m_title << ']' << '(' << buf << "/train/cpp" << ")\n";
				ofs << "```cpp\n";
				while (std::getline(ifs, buf)) {
					ofs << buf << "\n";
				}
				ifs.close();
				ofs << "```\n";
			}
			ofs << "---" << "\n";
		}

	} while (--curkyu.level);
	ofs.close();
}
int main() {
#ifdef _WIN32
	//控制檯顯示亂碼糾正
	system("chcp 65001"); //設定字符集（使用SetConsoleCP(65001)設定無效，原因未知）
	//SetConsoleOutputCP(65001);
#endif

	RWfile();

	namespace cp = crossplatform;
	cp::pause();
	return 0;
}