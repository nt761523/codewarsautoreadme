#include "crossplatform.h"

void crossplatform::pause() {
#ifdef __linux__
	int ch = 0;
	std::cout << "\nPress any key to continue . . . ";
	do {
		ch = getch();
	} while (ch != 0);
	std::cout << std::endl;
#elif _WIN32
	std::system("pause");
#else

#endif // __linux__

}
void crossplatform::clear() {
#ifdef __linux__
	std::system("clear");
#elif _WIN32
	std::system("cls");
#else
#endif // __linux__
}